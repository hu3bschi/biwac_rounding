<?php
class Biwac_Rounding_Model_Rewrite_Mage_Sales_Model_Quote_Address_Total_Grand extends Mage_Sales_Model_Quote_Address_Total_Grand
{
    /**
     * Collect grand total address amount
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Grand
     */
    public function collect(Mage_Sales_Model_Quote_Address $address) {

        $grandTotal     = $address->getGrandTotal();
        $baseGrandTotal = $address->getBaseGrandTotal();

        $totals     = array_sum($address->getAllTotalAmounts());
        $baseTotals = array_sum($address->getAllBaseTotalAmounts());

        /* @var $rounding_helper Biwac_Rounding_Helper_Data */
        $rounding_helper = Mage::helper('Biwac_Rounding');

        if($rounding_helper->apply('totals')){
            $address->setGrandTotal($rounding_helper->round($grandTotal+$totals));
            $address->setBaseGrandTotal($rounding_helper->round($baseGrandTotal+$baseTotals));
            return this;
        }

        $address->setGrandTotal($grandTotal+$totals);
        $address->setBaseGrandTotal($baseGrandTotal+$baseTotals);
        return $this;
    }
}
