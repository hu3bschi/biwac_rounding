<?php
class Biwac_Rounding_Helper_Data extends Mage_Core_Helper_Abstract {

    const DEFAULT_ALLOWED_CURRENCY_CODE = 'CHF';

    /**
     * Rundung
     * @return numeric
     */
    public function round($value){
        return round($value*20,0)/20;
    }


    /**
     * @return boolean
     */
    public function apply($where){

        if ($this->applyForCurrentCurrency() === false){
            return false;
        }

        if ($this->applyFor($where) === false){
            return false;
        }

        return true;
    }

    /**
     * True wenn die aktuelle Währung mit der konfigurierten Währung übereinstimmt.
     * @return boolean
     */
    protected function applyForCurrentCurrency(){

        $allowed_currency = self::DEFAULT_ALLOWED_CURRENCY_CODE;

        if($this->getCurrentCurrency() == $allowed_currency){
            return true;
        }
        return false;
    }

    protected function applyFor($where){

        if($where == 'totals'){
            return true;
        }
        return false;

    }

    /**
     * Aktuelle StoreView-Währung laden
     */
    private function getCurrentCurrency(){

        return Mage::app()->getStore()->getCurrentCurrencyCode();
    }

}