﻿##################################
# 
# Biwac_Rounding Extension
# 22.10.2012
#
# @author Christian Hübschi <christian.huebschi@biwac.ch>
#
# In der derzeitigen Version wird das Gesamttotal im Warenkorb bis zu der Bestellung auf 5 Rappen gerundet.
# Dir Rundung wird nur im Quote und für CHF angewendet.
# 
# 
#
#
# Entwickelt und getestet auf Magento CE 1.6.2


